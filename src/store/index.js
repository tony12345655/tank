import { createStore } from 'vuex'

export default createStore({
  state: {
    score:null,
  },
  actions: {
    EnemyDestroy(context,{num}){
      context.commit("addScore", num)
    }
  },
  mutations: {
    addScore(state, num){
      state.score = num
    }
  },
  getters: {
    score(state){
      return state.score
    }
  }
})
